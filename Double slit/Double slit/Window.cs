﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;

namespace Double_slit
{

    public partial class Window : Form
    {
        public const int W = 100, H = 100, interpolatedW = 100;
        public const int iW = 500, iH = 500;
        public const int iX = 250, iY = 215;
        private Bitmap surface;
        private Bitmap interpolatedSurface;
        private Bitmap accumulatedSurface;

        BufferedGraphicsContext bufgcont = new BufferedGraphicsContext();
        BufferedGraphics bufgraph;

        BufferedGraphicsContext interpolatedBufgcont = new BufferedGraphicsContext();
        BufferedGraphics interpolatedBufgraph;
        BufferedGraphics accumulationBufgraph;


        private double[,] height = new double[H, W];
        private double[,] velocity = new double[H, W];
        private double[,] acceleration = new double[H, W];
        private double[,] propagation = new double[H, W];
        private double[,] interpolatedHeight = new double[H, interpolatedW];
        private double[,] accumulatedHeight = new double[H, interpolatedW];

        int numOfInterpolations = 0;


        private Color staticParticle = Color.FromArgb(0, 0, 0);
        Color color1 = Color.FromArgb(70, 80, 92);
        Color color2 = Color.FromArgb(255, 255, 255);


        private Mutex mutex = new Mutex();

        // if walls or obstacles are added
        HashSet<(int, int)> statics = new HashSet<(int, int)>();

        // y, x of light particles
        HashSet<(int, int)> true_particles = new HashSet<(int, int)>();

        // y, x of oscillator points
        HashSet<(int, int)> oscillators = new HashSet<(int, int)>();

        // triangles that represent slits
        HashSet<PictureBox> slits = new HashSet<PictureBox>();

        // phase of each oscillator
        Dictionary<(int, int), double> phases = new Dictionary<(int, int), double>();


        double frequency = 0.2F;
        double power = 1F, mass = 0.1F;
        double height_limit = 400;
        double normalization = 20F;

        // for slit spacing
        double spacing = 1;

        private bool on = false;
        private bool touched = false, period = false, capture = false, was = false;

        Thread calculations;
        Thread interpolation;
        
        public Window()
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            surface = new Bitmap(W, H, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            interpolatedSurface = new Bitmap(interpolatedW, H, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            accumulatedSurface = new Bitmap(interpolatedW, H, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            bufgraph = bufgcont.Allocate(CreateGraphics(), new Rectangle(iX, iY, iW, iH));
            interpolatedBufgraph = bufgcont.Allocate(CreateGraphics(), new Rectangle(845, 216, 100, 500));
            accumulationBufgraph = bufgcont.Allocate(CreateGraphics(), new Rectangle(980, 216, 100, 500));
            InitializeComponent();
            createSlits();

            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < interpolatedW; j++)
                {
                    accumulatedHeight[i, j] = 0;
                }
            }

            calculations = new Thread(calculate);
            interpolation = new Thread(interpolate);
            calculations.Start();
            interpolation.Start();
        }

        // changes oscialltor y, x from y, -1 to y, 0 to start
        private void start()
        {
            (int, int) modified;
            HashSet<(int, int)> oscillators2 = new HashSet<(int, int)>(oscillators);
            foreach ((int, int) par in oscillators2)
            {
                oscillators.Remove((par.Item1, par.Item2));
                modified = (par.Item1, 0);
                oscillators.Add(modified);
                if(cameraCheckBox.Checked)
                {
                    true_particles.Add(modified);
                }
            }    
        }

        // changes oscillator y, 0 to y, -1 to stop
        private void stop()
        {
            (int, int) modified;
            HashSet<(int, int)> oscillators2 = new HashSet<(int, int)>(oscillators);
            foreach ((int, int) par in oscillators2)
            {
                oscillators.Remove((par.Item1, par.Item2));
                //height[par.Item1, par.Item2] *= -1;
                modified = (par.Item1, -1);
                oscillators.Add(modified);
            }
            true_particles.Clear();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            mutex.WaitOne();
            if (on)
            {
                pictureBox1.Image = Double_slit.Properties.Resources.lightOff;
            }
            else
            {
                pictureBox1.Image = Double_slit.Properties.Resources.lightOn;
            }
            on = !on;
            touched = false;
            if (on)
            {
                start();
            }
            else
            {
                stop();
            }
            mutex.ReleaseMutex();
        }

        // calculates surface values
        private void calculate()
        {
            var gr = Graphics.FromImage(surface);
            calculatePropagation();
            while (true)
            {
                mutex.WaitOne();
                doCalculations();
                generateImage();
                bufgraph.Graphics.DrawImage(surface, iX, iY, iW, iH);
                bufgraph.Render();
                mutex.ReleaseMutex();
            }

        }

        private void createSlits()
        {
            BufferedGraphicsContext a1 = new BufferedGraphicsContext();
            BufferedGraphics gr1;
            Bitmap wall1;
            wall1 = new Bitmap(100, 100, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(wall1) ;
            g.Clear(Color.Green);
            gr1 = a1.Allocate(CreateGraphics(), new Rectangle(200, 200, 100, 100));
            gr1.Graphics.DrawImage(wall1, 200, 200, 100, 100);
            gr1.Render();
            oscillators.Add((40, -1));
            oscillators.Add((64, -1));
            phases.Add((40, 0), 0);
            phases.Add((64, 0), 0);
            drawSlits();
        }

        // creates propagation values that handle edges
        // the closer the y, x is to the edges, the smaller the value
        private void calculatePropagation()
        {
            double mini = 2F;
            double maxi = 1000F;
            double current = mini;
            double step = (maxi - mini) / 10;


            // fill
            for(int i = 0; i < H; i++)
            {
                for(int j = 0; j < W; j++)
                {
                    propagation[i, j] = maxi;
                }
            }

            //handles uppper and bottom edge
            for(int i = 0; i < 10; i++)
            {
                for(int j = 0; j < W; j++)
                {
                    propagation[i, j] = current;
                    propagation[H - 1 - i, j] = current;
                }
                current += step;
            }

            current = mini;

            
            // handles left and right edges
            for (int j = 0; j < 10; j++)
            {
                for (int i = 5; i < H - 5; i++)
                {
                    propagation[i, j] = current;
                    propagation[i, W - 1 - j] = current;
                }
                current += step;
            }

        }

        private void doCalculations()
        {
            int dynamic_around = 0; // num of dynamic particles
            double heights_around; // sum of heigths
            int p, q; // counters
            double total = 0; // total for the whole surface
            int k1, k2; // counters

            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < W; j++)
                {
                    if (statics.Contains((i, j))) continue;

                    // calculates phases and heights of the oscillators
                    if (oscillators.Contains((i, j)))
                    {
                        velocity[i, j] = 0;
                        acceleration[i, j] = 0;
                        height[i, j] = height_limit * Math.Sin(phases[(i, j)]);
                        phases[(i, j)] += frequency;
                        if (phases[(i, j)] > 2 * Math.PI) period = true;
                        else period = false;
                        phases[(i, j)] %= 2 * Math.PI;
                        continue;
                    }

                    acceleration[i, j] = 0;
                    total += height[i, j];
                    dynamic_around = 0;
                    heights_around = 0;

                    if(true_particles.Contains((i, j - 1))) // particle calculation
                    {
                        if (j - 1 < 0) continue; 
                        heights_around = height[i, j - 1];
                        dynamic_around = 4;
                        true_particles.Add((i, j));
                    }
                    else // wave calculation
                    {
                        // heights of surrounding 'particles'
                        for (p = 0; p < 3; p++)
                        {
                            for (q = 0; q < 3; q++)
                            {
                                k1 = i - 1 + p;
                                k2 = j - 1 + q;
                                if (true_particles.Contains((k1, k2))) // particles that behave like particles are skipped
                                {
                                    dynamic_around++;
                                    continue;
                                }
                                if (k1 >= 0 && k1 < H && k2 >= 0 && k2 < W && !(i == 1 && j == 1))
                                {
                                    heights_around += height[k1, k2];
                                    dynamic_around++;
                                }
                            }
                        }
                    }

                    // calculates acceleration using sencond Newtons law
                    if (dynamic_around != 0)
                    {
                        heights_around /= dynamic_around;
                        acceleration[i, j] += Math.Sign(heights_around - height[i, j]) * Math.Pow(Math.Abs(height[i, j] - heights_around), power) / mass;
                    }

                    // edges are handled
                    acceleration[i, j] -= velocity[i, j]/propagation[i, j];

                }
            }
            double shifting = -1 * total / (W * H); // inertia
            double normalized_v;
            // normalization of height
            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < W; j++)
                {
                    velocity[i, j] += acceleration[i, j];
                    normalized_v = velocity[i, j] / normalization;

                    if (height[i, j] + normalized_v > height_limit)
                    {
                        height[i, j] = height_limit;
                    }
                    else if (height[i, j] + normalized_v <= height_limit && height[i, j] + normalized_v >= -height_limit)
                    { 
                        height[i, j] += normalized_v;
                    }
                    else
                    {
                        height[i, j] = -height_limit;
                    }

                    height[i, j] += shifting;
                }
            }      

        }

        private void frequencyTrackBar_ValueChanged(object sender, EventArgs e)
        {
            mutex.WaitOne();
            frequency = (double)frequencyTrackBar.Value / 10;
            mutex.ReleaseMutex();
        }

        private void slotNumberTrackBar_ValueChanged(object sender, EventArgs e)
        {
            int step = W / (slotNumberTrackBar.Value + 1);
            oscillators.Clear();
            int current;
            step = (int)Math.Floor(step * spacing);
            for (int i = 0; i < slotNumberTrackBar.Value; i++)
            {
                current = ((calculateSlitYLocation(i, slotNumberTrackBar.Value) - 216) / 5) + 3;
                Console.WriteLine(current);
                if(on) oscillators.Add((current, 0));
                else oscillators.Add((current, -1));
                phases[(current, 0)] = 0;
            }
            drawSlits();
        }

        private void drawSlits()
        {
            int counter = -1;
            foreach (PictureBox pb in slits)
            {
                Controls.Remove(pb);
            }
            slits.Clear();
            true_particles.Clear();
            foreach ((int, int) val in oscillators)
            {
                counter++;
                PictureBox pb = new PictureBox
                {
                    Size = new Size(25, 25),
                    Location = new Point(iX - 25, calculateSlitYLocation(counter, oscillators.Count)),
                    Image = Double_slit.Properties.Resources.slit,
                    SizeMode = PictureBoxSizeMode.StretchImage,
                };
                Controls.Add(pb);
                slits.Add(pb);
                if (cameraCheckBox.Checked)
                {
                    true_particles.Add(val);
                }
            }

        }

        private int calculateSlitYLocation(int numberOfCurrentSlit, int numberOfSlits)
        {
            int middleHeight = iY + iW / 2;
            int spaceBetweenSlits = (int)(spacing * 60);
            if (numberOfSlits % 2 == 1)
            {
                if (numberOfCurrentSlit == 0)
                {
                    return middleHeight;
                }
                else if (numberOfCurrentSlit % 2 == 0)
                {
                    return (middleHeight - numberOfCurrentSlit * spaceBetweenSlits);
                }
                else
                {
                    return (middleHeight + (numberOfCurrentSlit + 1) * spaceBetweenSlits);
                }
            }
            else
            {
                if (numberOfCurrentSlit % 2 == 0)
                {
                    return (middleHeight - (numberOfCurrentSlit + 1) * spaceBetweenSlits);
                }
                else
                {
                    return (middleHeight + (numberOfCurrentSlit) * spaceBetweenSlits);
                }
            }
        }


        private void slotSpaceTrackBar_ValueChanged(object sender, EventArgs e)
        {
            mutex.WaitOne();
            spacing = slotSpaceTrackBar.Value*1.0 / 100;
            slotNumberTrackBar_ValueChanged(sender, e);
            mutex.ReleaseMutex();

        }

        private void massTrackBar_ValueChanged(object sender, EventArgs e)
        {
            mutex.WaitOne();
            mass = massTrackBar.Value / 100F;
            mutex.ReleaseMutex();
        }

        private void lightIntensityTrackBar_ValueChanged(object sender, EventArgs e)
        {
            mutex.WaitOne();
            power = lightIntensityTrackBar.Value / 10F;
            mutex.ReleaseMutex();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mutex.WaitOne();
            pictureBox1.Image = Double_slit.Properties.Resources.lightOff;
            stop();
            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < W; j++)
                {
                    height[i, j] = 0;
                    acceleration[i, j] = 0;
                    velocity[i, j] = 0;
                }
            }
            on = false;
            touched = false;
            mutex.ReleaseMutex();
        }

        private void Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            mutex.WaitOne();
            calculations.Abort();
            interpolation.Abort();
            mutex.ReleaseMutex();
        }

        private void generateImage()
        {
            var bitmapData = surface.LockBits(new Rectangle(0, 0, surface.Width, surface.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, surface.PixelFormat);
            var length = bitmapData.Stride * bitmapData.Height;
            byte[] bytes = new byte[length];

            int k = 0;
            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < W; j++)
                {
                    byte heightBright = (byte)((height[i, j] + height_limit) / (height_limit * 2f / 255f));
                    if (statics.Contains((i, j))) continue;

                    double b1 = heightBright / 255f;
                    double b2 = 1f - heightBright / 255f;
                    bytes[k] = (byte)(color1.B * b1 + color2.B * b2);
                    bytes[k + 1] = (byte)(color1.G * b1 + color2.G * b2);
                    bytes[k + 2] = (byte)(color1.R * b1 + color2.R * b2);
                    k += 3;
                }
            }

            System.Runtime.InteropServices.Marshal.Copy(bytes, 0, bitmapData.Scan0, length);
            surface.UnlockBits(bitmapData);
        }

        private void interpolate()
        {
            var gr = Graphics.FromImage(interpolatedSurface);
            while (true)
            {
                mutex.WaitOne();
                doInterpolation();
                generateInterpolatedImage();
                calculateAccumulation();
                generateAccumulatedImage();
                interpolatedBufgraph.Graphics.DrawImage(interpolatedSurface, 845, 216, 100, 500);
                interpolatedBufgraph.Render(); 
                accumulationBufgraph.Graphics.DrawImage(accumulatedSurface, 980, 216, 100, 500);
                accumulationBufgraph.Render();
                mutex.ReleaseMutex();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mutex.WaitOne();
            
            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < interpolatedW; j++)
                {   
                    accumulatedHeight[i, j] = 0;
                }
            }

            mutex.ReleaseMutex();
        }

        private void doInterpolation()
        {
            // interpolation happens here
            double widthDifference;
            for (int i = 0; i < H; i++)
            {
                interpolatedHeight[i, 0] = height[i, W - 1] * .5; //.8
                interpolatedHeight[i, interpolatedW / 2] = height[i, W - 1];
                interpolatedHeight[i, interpolatedW - 1] = height[i, W - 1] * .5;
                if (height[i, W - 1] > height_limit / 16) touched = true;
            }
            for (int i = 0; i < H; i++)
            {
                for (int j = 1; j < interpolatedW; j++)
                {
                    if (j <= interpolatedW / 2)
                    {
                        widthDifference = interpolatedHeight[i, interpolatedW / 2] - interpolatedHeight[i, 0];
                        widthDifference /= (interpolatedW / 2);
                        interpolatedHeight[i, j] = interpolatedHeight[i, j - 1] + widthDifference;
                    }
                    else
                    {
                        widthDifference = interpolatedHeight[i, interpolatedW - 1] - interpolatedHeight[i, interpolatedW / 2];
                        widthDifference /= (interpolatedW / 2);
                        interpolatedHeight[i, j] = interpolatedHeight[i, j - 1] + widthDifference;
                    }
                }
            }

        }

        private void generateInterpolatedImage()
        {
            var bitmapData = interpolatedSurface.LockBits(new Rectangle(0, 0, interpolatedSurface.Width, interpolatedSurface.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, interpolatedSurface.PixelFormat);
            var length = bitmapData.Stride * bitmapData.Height;
            byte[] bytes = new byte[length];

            int k = 0;
            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < interpolatedW; j++)
                {
                    double bright;
                    bright = Math.Abs(interpolatedHeight[i, j] * 2 / height_limit); // 2
                 
                    if (statics.Contains((i, j))) continue;

                    double b2 = bright;
                    double b1 = 1 - b2;
                    bytes[k] = (byte)(Color.Black.B * b1 + Color.White.B * b2);
                    bytes[k + 1] = (byte)(Color.Black.G * b1 + Color.White.G * b2);
                    bytes[k + 2] = (byte)(Color.Black.R * b1 + Color.White.R * b2);
                    k += 3;
                }
            }

            System.Runtime.InteropServices.Marshal.Copy(bytes, 0, bitmapData.Scan0, length);
            interpolatedSurface.UnlockBits(bitmapData);
        }

        
        private void calculateAccumulation()
        {
            if (!on || !touched) return;
            if (capture == true && period == true) capture = false;
            if (period == true && capture == false) { capture = true; was = true; }

            if (!capture) return;
          
            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < interpolatedW; j++)
                {
                    if(interpolatedHeight[i, j] > 0)
                    {
                        accumulatedHeight[i, j] = accumulatedHeight[i, j] + Math.Abs(interpolatedHeight[i, j])*0.02;
                        if (accumulatedHeight[i, j] > height_limit) accumulatedHeight[i, j] = height_limit;
                    }
                }
            }
            ++numOfInterpolations;
        }

        private void generateAccumulatedImage()
        {
            var bitmapData = accumulatedSurface.LockBits(new Rectangle(0, 0, accumulatedSurface.Width, accumulatedSurface.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, accumulatedSurface.PixelFormat);
            var length = bitmapData.Stride * bitmapData.Height;
            byte[] bytes = new byte[length];

            int k = 0;
            for (int i = 0; i < H; i++)
            {
                for (int j = 0; j < interpolatedW; j++)
                {
                    double bright = Math.Abs(accumulatedHeight[i, j] / height_limit);
                    if (statics.Contains((i, j))) continue;

                    double b2 = bright;
                    double b1 = 1 - b2;
                    bytes[k] = (byte)(Color.Black.B * b1 + Color.White.B * b2);
                    bytes[k + 1] = (byte)(Color.Black.G * b1 + Color.White.G * b2);
                    bytes[k + 2] = (byte)(Color.Black.R * b1 + Color.White.R * b2);
                    k += 3;
                }
            }

            System.Runtime.InteropServices.Marshal.Copy(bytes, 0, bitmapData.Scan0, length);
            accumulatedSurface.UnlockBits(bitmapData);
        }



        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            mutex.WaitOne();
            if (cameraCheckBox.Checked)
            {
                cameraPictureBox.Image = Double_slit.Properties.Resources.cameraOnLittle;
                foreach ((int, int) par in oscillators)
                {
                    true_particles.Add((par.Item1, par.Item2));
                }
            }
            else
            {
                cameraPictureBox.Image = Double_slit.Properties.Resources.cameraOffLittle;
                true_particles.Clear();
            }
            mutex.ReleaseMutex();
        }


    }
}