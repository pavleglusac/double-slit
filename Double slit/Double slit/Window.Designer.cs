﻿using System.Windows.Forms;

namespace Double_slit
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.label2 = new System.Windows.Forms.Label();
            this.cameraCheckBox = new System.Windows.Forms.CheckBox();
            this.slotSpaceTrackBar = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.massTrackBar = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lightIntensityTrackBar = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.frequencyTrackBar = new System.Windows.Forms.TrackBar();
            this.label6 = new System.Windows.Forms.Label();
            this.slotNumberTrackBar = new System.Windows.Forms.TrackBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cameraPictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.slotSpaceTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.massTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightIntensityTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slotNumberTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(1096, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Posmatrač:";
            // 
            // cameraCheckBox
            // 
            this.cameraCheckBox.Location = new System.Drawing.Point(1197, 19);
            this.cameraCheckBox.Name = "cameraCheckBox";
            this.cameraCheckBox.Size = new System.Drawing.Size(37, 38);
            this.cameraCheckBox.TabIndex = 7;
            this.cameraCheckBox.UseVisualStyleBackColor = true;
            this.cameraCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // slotSpaceTrackBar
            // 
            this.slotSpaceTrackBar.LargeChange = 10;
            this.slotSpaceTrackBar.Location = new System.Drawing.Point(83, 12);
            this.slotSpaceTrackBar.Maximum = 100;
            this.slotSpaceTrackBar.Minimum = 30;
            this.slotSpaceTrackBar.Name = "slotSpaceTrackBar";
            this.slotSpaceTrackBar.Size = new System.Drawing.Size(142, 45);
            this.slotSpaceTrackBar.SmallChange = 10;
            this.slotSpaceTrackBar.TabIndex = 8;
            this.slotSpaceTrackBar.Value = 100;
            this.slotSpaceTrackBar.ValueChanged += new System.EventHandler(this.slotSpaceTrackBar_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(67, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Razmak između proreza";
            // 
            // massTrackBar
            // 
            this.massTrackBar.Location = new System.Drawing.Point(292, 12);
            this.massTrackBar.Maximum = 50;
            this.massTrackBar.Minimum = 3;
            this.massTrackBar.Name = "massTrackBar";
            this.massTrackBar.Size = new System.Drawing.Size(142, 45);
            this.massTrackBar.TabIndex = 10;
            this.massTrackBar.Value = 10;
            this.massTrackBar.ValueChanged += new System.EventHandler(this.massTrackBar_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(305, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Masa čestice";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(479, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "Intenzitet izvora svestlosti";
            // 
            // lightIntensityTrackBar
            // 
            this.lightIntensityTrackBar.Location = new System.Drawing.Point(498, 12);
            this.lightIntensityTrackBar.Maximum = 11;
            this.lightIntensityTrackBar.Minimum = 5;
            this.lightIntensityTrackBar.Name = "lightIntensityTrackBar";
            this.lightIntensityTrackBar.Size = new System.Drawing.Size(142, 45);
            this.lightIntensityTrackBar.TabIndex = 12;
            this.lightIntensityTrackBar.Value = 5;
            this.lightIntensityTrackBar.ValueChanged += new System.EventHandler(this.lightIntensityTrackBar_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(726, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "Frekvencija";
            // 
            // frequencyTrackBar
            // 
            this.frequencyTrackBar.LargeChange = 2;
            this.frequencyTrackBar.Location = new System.Drawing.Point(701, 12);
            this.frequencyTrackBar.Name = "frequencyTrackBar";
            this.frequencyTrackBar.Size = new System.Drawing.Size(142, 45);
            this.frequencyTrackBar.TabIndex = 14;
            this.frequencyTrackBar.Value = 2;
            this.frequencyTrackBar.ValueChanged += new System.EventHandler(this.frequencyTrackBar_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(922, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "Broj proreza";
            // 
            // slotNumberTrackBar
            // 
            this.slotNumberTrackBar.LargeChange = 1;
            this.slotNumberTrackBar.Location = new System.Drawing.Point(897, 12);
            this.slotNumberTrackBar.Maximum = 4;
            this.slotNumberTrackBar.Minimum = 1;
            this.slotNumberTrackBar.Name = "slotNumberTrackBar";
            this.slotNumberTrackBar.Size = new System.Drawing.Size(142, 45);
            this.slotNumberTrackBar.TabIndex = 16;
            this.slotNumberTrackBar.Value = 2;
            this.slotNumberTrackBar.ValueChanged += new System.EventHandler(this.slotNumberTrackBar_ValueChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Double_slit.Properties.Resources.lightOff;
            this.pictureBox1.Location = new System.Drawing.Point(6, 388);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(219, 153);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // cameraPictureBox
            // 
            this.cameraPictureBox.Image = global::Double_slit.Properties.Resources.cameraOffLittle;
            this.cameraPictureBox.Location = new System.Drawing.Point(71, 156);
            this.cameraPictureBox.Name = "cameraPictureBox";
            this.cameraPictureBox.Size = new System.Drawing.Size(154, 111);
            this.cameraPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cameraPictureBox.TabIndex = 18;
            this.cameraPictureBox.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Black;
            this.pictureBox4.Location = new System.Drawing.Point(740, 216);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 500);
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1153, 714);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Reset accumulation";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 714);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(105, 23);
            this.button2.TabIndex = 21;
            this.button2.Text = "Reset surface";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 749);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cameraPictureBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.slotNumberTrackBar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.frequencyTrackBar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lightIntensityTrackBar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.massTrackBar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.slotSpaceTrackBar);
            this.Controls.Add(this.cameraCheckBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Window";
            this.Text = "Window";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Window_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.slotSpaceTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.massTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightIntensityTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slotNumberTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cameraCheckBox;
        private System.Windows.Forms.TrackBar slotSpaceTrackBar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar massTrackBar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar lightIntensityTrackBar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar frequencyTrackBar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TrackBar slotNumberTrackBar;
        private System.Windows.Forms.PictureBox cameraPictureBox;
        private PictureBox pictureBox1;
        private Button button1;
        private Button button2;
    }
}

